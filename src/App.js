import React , { Component}  from 'react';
import './App.css';

function App() {

  function read(){
    const myValueInLocalStorage = localStorage.getItem("chats");
    document.getElementById("example").value = myValueInLocalStorage;  
    console.log(myValueInLocalStorage)
  }
  function write(){
    localStorage.setItem("chats", document.getElementById("example").value);
  }
  return (
    <div className="App">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultrices in iaculis nunc sed augue lacus viverra. Eu augue ut lectus arcu. Urna condimentum mattis pellentesque id nibh tortor. Tempus iaculis urna id volutpat. Vestibulum morbi blandit cursus risus at ultrices mi tempus. Ultricies leo integer malesuada nunc vel risus commodo viverra maecenas. In mollis nunc sed id semper risus in. Sed turpis tincidunt id aliquet. Nec tincidunt praesent semper feugiat nibh sed. Mattis rhoncus urna neque viverra justo nec ultrices dui. In aliquam sem fringilla ut morbi tincidunt augue interdum velit.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultrices in iaculis nunc sed augue lacus viverra. Eu augue ut lectus arcu. Urna condimentum mattis pellentesque id nibh tortor. Tempus iaculis urna id volutpat. Vestibulum morbi blandit cursus risus at ultrices mi tempus. Ultricies leo integer malesuada nunc vel risus commodo viverra maecenas. In mollis nunc sed id semper risus in. Sed turpis tincidunt id aliquet. Nec tincidunt praesent semper feugiat nibh sed. Mattis rhoncus urna neque viverra justo nec ultrices dui. In aliquam sem fringilla ut morbi tincidunt augue interdum velit.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultrices in iaculis nunc sed augue lacus viverra. Eu augue ut lectus arcu. Urna condimentum mattis pellentesque id nibh tortor. Tempus iaculis urna id volutpat. Vestibulum morbi blandit cursus risus at ultrices mi tempus. Ultricies leo integer malesuada nunc vel risus commodo viverra maecenas. In mollis nunc sed id semper risus in. Sed turpis tincidunt id aliquet. Nec tincidunt praesent semper feugiat nibh sed. Mattis rhoncus urna neque viverra justo nec ultrices dui. In aliquam sem fringilla ut morbi tincidunt augue interdum velit.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultrices in iaculis nunc sed augue lacus viverra. Eu augue ut lectus arcu. Urna condimentum mattis pellentesque id nibh tortor. Tempus iaculis urna id volutpat. Vestibulum morbi blandit cursus risus at ultrices mi tempus. Ultricies leo integer malesuada nunc vel risus commodo viverra maecenas. In mollis nunc sed id semper risus in. Sed turpis tincidunt id aliquet. Nec tincidunt praesent semper feugiat nibh sed. Mattis rhoncus urna neque viverra justo nec ultrices dui. In aliquam sem fringilla ut morbi tincidunt augue interdum velit.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultrices in iaculis nunc sed augue lacus viverra. Eu augue ut lectus arcu. Urna condimentum mattis pellentesque id nibh tortor. Tempus iaculis urna id volutpat. Vestibulum morbi blandit cursus risus at ultrices mi tempus. Ultricies leo integer malesuada nunc vel risus commodo viverra maecenas. In mollis nunc sed id semper risus in. Sed turpis tincidunt id aliquet. Nec tincidunt praesent semper feugiat nibh sed. Mattis rhoncus urna neque viverra justo nec ultrices dui. In aliquam sem fringilla ut morbi tincidunt augue interdum velit.</p>
      {/* this is just example implement according to the given task */}
      <div className="chatbox">
         <input type="text" id="example"></input>
         <button onClick={write}>Send</button>
         <button onClick={read}>Show</button>
      </div>
    </div>
  );
}

export default App;
